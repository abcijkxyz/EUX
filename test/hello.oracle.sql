-- EDITULTRA BEGIN DATABASE CONNECTION CONFIG
--  DBTYPE : Oracle
--  DBHOST : CALVINDB
--  DBPORT : 0
--  DBUSER : calvin
--  DBPASS : 
--  DBNAME : calvindb
-- EDITULTRA END DATABASE CONNECTION CONFIG

CREATE TABLE "test_editultra"  (
  tid NUMERIC(10,0) NOT NULL,
  trans_jnls_no VARCHAR2(20) NOT NULL,
  trans_code VARCHAR2(16) NOT NULL,
  trans_desc VARCHAR2(60) NULL,
  response_code VARCHAR2(6) NULL,
  trans_amt NUMERIC(12,2) NULL,
  trans_timestamp VARCHAR2(10)
);

CREATE UNIQUE INDEX "test_editultra_idx_1" ON "test_editultra" ( tid );

SELECT * FROM "test_editultra";

INSERT INTO "test_editultra" VALUES ( 1 , '2020062100000001' , 'APPC0001' , '交易A' , 0 , 100.00 , '159000001' );
INSERT INTO "test_editultra" VALUES ( 2 , '2020062100000002' , 'APPC0001' , '交易A' , 0 , 100.00 , '159000002' );
INSERT INTO "test_editultra" VALUES ( 3 , '2020062100000003' , 'APPC0001' , '交易A' , 0 , 100.00 , '159000003' );
INSERT INTO "test_editultra" VALUES ( 4 , '2020062100000004' , 'APPC0001' , '交易A' , 0 , 100.00 , '159000004' );
INSERT INTO "test_editultra" VALUES ( 5 , '2020062100000005' , 'APPC0001' , '交易A' , 0 , 100.00 , '159000005' );
INSERT INTO "test_editultra" VALUES ( 7 , '2020062100000007' , 'APPC0001' , NULL , NULL , NULL , NULL );

DELETE FROM "test_editultra";

SELECT * FROM "test_editultr";
